using UnityEngine;

public class ComputerPaddle : Paddle
{
    public Rigidbody2D Ball;
    
    //FixedUpdate is very useful for physics-related logic
    private void FixedUpdate()
    {
        if(this.Ball.velocity.x > 0.0f)
        {
            if (this.Ball.position.y > this.transform.position.y)
            {
                _RigidBody2D.AddForce(Vector2.up * this.Speed);
            } 
            else if (this.Ball.position.y < this.transform.position.y)
            {
                _RigidBody2D.AddForce(Vector2.down * this.Speed);
            }
        }
        else
        {
            if (this.transform.position.y > 0.0f)
            {
                _RigidBody2D.AddForce(Vector2.down * this.Speed);
            }
            else if (this.transform.position.y < 0.0f)
            {
                _RigidBody2D.AddForce(Vector2.up * this.Speed);
            }
        }
    }
}
