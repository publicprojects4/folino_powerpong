using UnityEngine;

public class Ball : MonoBehaviour
{
    public float Speed = 600.0f;
    
    public Rigidbody2D _RigidBody2D;

    private float x;
    private float y;

    private void Awake()
    {
        _RigidBody2D = GetComponent<Rigidbody2D>();
    }


    private void Start()
    {
        ResetPosition();
        AddStartingForce();
    }


    public void ResetPosition()
    {
        _RigidBody2D.position = Vector3.zero;
        _RigidBody2D.velocity = Vector3.zero;

    }


    //The following will choose a random direction and throw the ball along it
    public void AddStartingForce() 
    {
        /*The following is like a coin flip: if the Random.value returns a value less than 0.5
        is a state (like negative), otherwise is the other state (like, positive)*/
        
        float x = Random.value < 0.5f ? -0.7f : 0.7f;

        
        float y = Random.value < 0.5f ? Random.Range(-0.7f, -0.5f) :
                                        Random.Range(0.5f, 0.7f);

        /*var arrayX = new float[] { -1f, 1f };
        var arrayY = new float[] { -0.7f, -0.5f, -0.3f, 0.3f, 0.5f, 0.7f };
        float x = arrayX[Random.Range(0, arrayX.Length)];
        float y = arrayY[Random.Range(0, arrayY.Length)];*/

        //The vector we created set the direction of the ball
        Vector2 _Direction = new Vector2(x, y);
        _RigidBody2D.AddForce(_Direction.normalized * this.Speed);

        Debug.Log(_Direction.normalized);
    }


    public void AddForce(Vector2 force)
    {
        _RigidBody2D.AddForce(force);
    }

    
}
