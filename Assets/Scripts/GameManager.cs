using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public Ball ball;
    public Text PlayerScoreText;
    public Text ComputerScoreText;

    public Paddle PlayerPaddle;
    public Paddle ComputerPaddle;


    private int _PlayerScore;
    private int _ComputerScore;
    public int EndScore;

    private void FixedUpdate()
    {
        CheckEndScore();
    }

    public void PlayerScores()
    {
        _PlayerScore++ ;
        ResetRound();
    }

    public void ComputerScore()
    {
        _ComputerScore++ ;
        ResetRound();
        
    }

    private void ResetRound()
    {
        this.PlayerPaddle.ResetPosition();
        this.ComputerPaddle.ResetPosition();
        this.PlayerScoreText.text = _PlayerScore.ToString();
        this.ComputerScoreText.text = _ComputerScore.ToString();
        this.ball.ResetPosition();
        this.ball.AddStartingForce();
    }

    private void CheckEndScore()
    {
        if (_PlayerScore == EndScore)
        {
            ResetGame();
        }
        else if (_ComputerScore == EndScore)
        {
            ResetGame();
        }
    }

    private void ResetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
