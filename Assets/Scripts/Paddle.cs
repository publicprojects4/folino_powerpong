using UnityEngine;

public class Paddle : MonoBehaviour
{
    public float Speed = 10.0f;
    
    protected Rigidbody2D _RigidBody2D;

    private void Awake()
    {
        _RigidBody2D = GetComponent<Rigidbody2D>();
    }

    public void ResetPosition()
    {
        _RigidBody2D.position = new Vector2(_RigidBody2D.position.x, 0.0f);
        _RigidBody2D.velocity = Vector2.zero;
    }
}
