using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public int numberToSpawn;
    public List<GameObject> spawnPool;
    public GameObject quad;
    public GameObject toSpawn;


    // Start is called before the first frame update
    void Start()
    {
        spawnObjects();
        
    }

    public void spawnObjects()
    {
        destroyObjects();

        int randomItem = 0;
        
        MeshCollider collider = quad.GetComponent<MeshCollider>();

        float screenX, screenY;
        Vector2 position;

        for (int i = 0; i < numberToSpawn; i++) 
        {
            randomItem = Random.Range(0, spawnPool.Count);
            toSpawn = spawnPool[randomItem];

            screenX = 0;
            screenY = Random.Range(collider.bounds.min.y, collider.bounds.max.y);
            position = new Vector2(screenX, screenY);

            Instantiate(toSpawn, position, toSpawn.transform.rotation);
        }
    }

    private void destroyObjects()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Spawnable"))
        {
            Destroy(obj);
        }
    }

    

    

}
